module.exports = [
	{
		letter:'a',
		words:[
			{
				index:0,
				word:"adam",
				original:"Адам" ,
				image:"/img/learn/word_1/word_11.svg"
			},
			{
				index:1,
				word:"alma",
				original:"Алма" ,
				image:"/img/learn/word_1/word_12.svg"
			},			
			{
				index:2,
				word:"ata",
				original:"Ата" ,
				image:"/img/learn/word_1/word_13.svg"
			},			
		]
	},
	{
		letter:'á',
		words:[
			{
				index:0,
				word:"átesh",
				original:"әтеш" ,
				image:"/img/learn/word_2/word_21.svg"
			},
			{
				index:1,
				word:"álippe",
				original:"әліппе" ,
				image:"/img/learn/word_2/word_22.svg"
			},			
			{
				index:2,
				word:"ásker",
				original:"әскер" ,
				image:"/img/learn/word_2/word_23.svg"
			},			
		]
	},	
	{
		letter:'b',
		words:[
			{
				index:0,
				word:"bala",
				original:"бала" ,
				image:"/img/learn/word_3/word_31.svg"
			},
			{
				index:1,
				word:"balapan",
				original:"балапан" ,
				image:"/img/learn/word_3/word_32.svg"
			},			
			{
				index:2,
				word:"bas",
				original:"бас" ,
				image:"/img/learn/word_3/word_33.svg"
			},			
		]
	},	
	{
		letter:'v',
		words:[
			{
				index:0,
				word:"vagon",
				original:"вагон" ,
				image:"/img/learn/word_4/word_41.svg"
			},
			{
				index:1,
				word:"velosıped",
				original:"велосипед" ,
				image:"/img/learn/word_4/word_42.svg"
			},			
			{
				index:2,
				word:"vanna",
				original:"ванна" ,
				image:"/img/learn/word_4/word_43.svg"
			},			
		]
	},	
	{
		letter:'g',
		words:[
			{
				index:0,
				word:"gúl",
				original:"гүл" ,
				image:"/img/learn/word_5/word_51.svg"
			},
			{
				index:1,
				word:"gazet",
				original:"газет" ,
				image:"/img/learn/word_5/word_52.svg"
			},			
			{
				index:2,
				word:"girtas",
				original:"гір-тас" ,
				image:"/img/learn/word_5/word_53.svg"
			},			
		]
	},	
	{
		letter:'ǵ',
		words:[
			{
				index:0,
				word:"ǵarysh",
				original:"ғарыш" ,
				image:"/img/learn/word_6/word_61.svg"
			},
			{
				index:1,
				word:"ǵımarat",
				original:"ғимарат" ,
				image:"/img/learn/word_6/word_62.svg"
			},			
			{
				index:2,
				word:"ǵalym",
				original:"ғалым" ,
				image:"/img/learn/word_6/word_63.svg"
			},			
		]
	},	
	{
		letter:'d',
		words:[
			{
				index:0,
				word:"dos",
				original:"дос" ,
				image:"/img/learn/word_7/word_71.svg"
			},
			{
				index:1,
				word:"dáriger",
				original:"дәрігер" ,
				image:"/img/learn/word_7/word_72.svg"
			},			
			{
				index:2,
				word:"dońyz",
				original:"доңыз" ,
				image:"/img/learn/word_7/word_73.svg"
			},			
		]
	},	
	{
		letter:'e',
		words:[
			{
				index:0,
				word:"esek",
				original:"есек" ,
				image:"/img/learn/word_8/word_81.svg"
			},
			{
				index:1,
				word:"el",
				original:"ел" ,
				image:"/img/learn/word_8/word_82.svg"
			},			
			{
				index:2,
				word:"erkin",
				original:"еркін" ,
				image:"/img/learn/word_8/word_83.svg"
			},			
		]
	},	
	{
		letter:'j',
		words:[
			{
				index:0,
				word:"jer",
				original:"жер" ,
				image:"/img/learn/word_9/word_91.svg"
			},
			{
				index:1,
				word:"jaýyńger",
				original:"жауыңгер" ,
				image:"/img/learn/word_9/word_92.svg"
			},			
			{
				index:2,
				word:"japalaq",
				original:"жапалақ" ,
				image:"/img/learn/word_9/word_93.svg"
			},			
		]
	},	
	{
		letter:'z',
		words:[
			{
				index:0,
				word:"zebra",
				original:"зебра" ,
				image:"/img/learn/word_10/word_101.svg"
			},
			{
				index:1,
				word:"zań",
				original:"заң" ,
				image:"/img/learn/word_10/word_102.svg"
			},			
			{
				index:2,
				word:"zaman",
				original:"заман" ,
				image:"/img/learn/word_10/word_103.svg"
			},			
		]
	},	
	{
		letter:'ı',
		words:[
			{
				index:0,
				word:"ıt",
				original:"ит" ,
				image:"/img/learn/word_11/word_111.svg"
			},
			{
				index:1,
				word:"ıis",
				original:"иіс" ,
				image:"/img/learn/word_11/word_112.svg"
			},			
			{
				index:2,
				word:"ıek",
				original:"иек" ,
				image:"/img/learn/word_11/word_113.svg"
			},			
		]
	},	
	{
		letter:'k',
		words:[
			{
				index:0,
				word:"kómek",
				original:"көмек" ,
				image:"/img/learn/word_12/word_121.svg"
			},
			{
				index:1,
				word:"kópir",
				original:"көпір" ,
				image:"/img/learn/word_12/word_122.svg"
			},			
			{
				index:2,
				word:"kún",
				original:"күн" ,
				image:"/img/learn/word_12/word_123.svg"
			},			
		]
	},	
	{
		letter:'q',
		words:[
			{
				index:0,
				word:"qoshaqan",
				original:"қошақан" ,
				image:"/img/learn/word_13/word_131.svg"
			},
			{
				index:1,
				word:"qarǵa",
				original:"қарға" ,
				image:"/img/learn/word_13/word_132.svg"
			},			
			{
				index:2,
				word:"qarlyǵash",
				original:"қарлығаш" ,
				image:"/img/learn/word_13/word_133.svg"
			},			
		]
	},	
	{
		letter:'l',
		words:[
			{
				index:0,
				word:"laq",
				original:"лақ" ,
				image:"/img/learn/word_14/word_141.svg"
			},
			{
				index:1,
				word:"las",
				original:"лас" ,
				image:"/img/learn/word_14/word_142.svg"
			},			
			{
				index:2,
				word:"laqap",
				original:"лақап" ,
				image:"/img/learn/word_14/word_143.svg"
			},			
		]
	},	
	{
		letter:'m',
		words:[
			{
				index:0,
				word:"maqala",
				original:"мақала" ,
				image:"/img/learn/word_15/word_151.svg"
			},
			{
				index:1,
				word:"maqta",
				original:"мақта" ,
				image:"/img/learn/word_15/word_152.svg"
			},			
			{
				index:2,
				word:"maımyl",
				original:"маймыл" ,
				image:"/img/learn/word_15/word_153.svg"
			},			
		]
	},	
	{
		letter:'n',
		words:[
			{
				index:0,
				word:"naızaǵaı",
				original:"найзағай" ,
				image:"/img/learn/word_16/word_161.svg"
			},
			{
				index:1,
				word:"naıza",
				original:"найза" ,
				image:"/img/learn/word_16/word_162.svg"
			},			
			{
				index:2,
				word:"náreste",
				original:"нәресте" ,
				image:"/img/learn/word_16/word_163.svg"
			},			
		]
	},	
	{
		letter:'ń',
		words:[
			{
				index:0,
				word:"tań",
				original:"таң" ,
				image:"/img/learn/word_31/word_311.svg"
			},
			{
				index:1,
				word:"mań",
				original:"маң" ,
				image:"/img/learn/word_31/word_312.svg"
			},			
			{
				index:2,
				word:"mańdaı",
				original:"маңдай" ,
				image:"/img/learn/word_31/word_313.svg"
			},			
		]
	},	
	{
		letter:'o',
		words:[
			{
				index:0,
				word:"oıyn",
				original:"ойын" ,
				image:"/img/learn/word_17/word_171.svg"
			},
			{
				index:1,
				word:"oılaý",
				original:"ойлау" ,
				image:"/img/learn/word_17/word_172.svg"
			},			
			{
				index:2,
				word:"o",
				original:"ою" ,
				image:"/img/learn/word_17/word_173.svg"
			},			
		]
	},	
	{
		letter:'ó',
		words:[
			{
				index:0,
				word:"ógiz",
				original:"өгіз" ,
				image:"/img/learn/word_18/word_181.svg"
			},
			{
				index:1,
				word:"ókimet",
				original:"өкімет" ,
				image:"/img/learn/word_18/word_182.svg"
			},			
			{
				index:2,
				word:"óleń",
				original:"өлең" ,
				image:"/img/learn/word_18/word_183.svg"
			},			
		]
	},	
	{
		letter:'p',
		words:[
			{
				index:0,
				word:"paraqsha",
				original:"парақша" ,
				image:"/img/learn/word_19/word_191.svg"
			},
			{
				index:1,
				word:"perızat",
				original:"перизат" ,
				image:"/img/learn/word_19/word_192.svg"
			},			
			{
				index:2,
				word:"pil",
				original:"піл" ,
				image:"/img/learn/word_19/word_193.svg"
			},			
		]
	},	
	{
		letter:'r',
		words:[
			{
				index:0,
				word:"radıoqabyldaǵysh",
				original:"радиоқабылдағыш" ,
				image:"/img/learn/word_20/word_201.svg"
			},
			{
				index:1,
				word:"raýshan",
				original:"раушан" ,
				image:"/img/learn/word_20/word_202.svg"
			},			
			{
				index:2,
				word:"raıhan",
				original:"райхан" ,
				image:"/img/learn/word_20/word_203.svg"
			},			
		]
	},	
	{
		letter:'s',
		words:[
			{
				index:0,
				word:"saǵat",
				original:"сағат" ,
				image:"/img/learn/word_21/word_211.svg"
			},
			{
				index:1,
				word:"salmaq",
				original:"салмақ" ,
				image:"/img/learn/word_21/word_212.svg"
			},			
			{
				index:2,
				word:"sarymsaq",
				original:"сарымсақ" ,
				image:"/img/learn/word_21/word_213.svg"
			},			
		]
	},	
	{
		letter:'t',
		words:[
			{
				index:0,
				word:"tabıǵat",
				original:"табиғат" ,
				image:"/img/learn/word_22/word_221.svg"
			},
			{
				index:1,
				word:"taǵa",
				original:"таға" ,
				image:"/img/learn/word_22/word_222.svg"
			},			
			{
				index:2,
				word:"tamshy",
				original:"тамшы" ,
				image:"/img/learn/word_22/word_223.svg"
			},			
		]
	},	
	{
		letter:'ý',
		words:[
			{
				index:0,
				word:"ýaqyt",
				original:"уақыт" ,
				image:"/img/learn/word_23/word_231.svg"
			},
			{
				index:1,
				word:"ýaqıǵa",
				original:"уақиға" ,
				image:"/img/learn/word_23/word_232.svg"
			},			
			{
				index:2,
				word:"ýáde",
				original:"уәде" ,
				image:"/img/learn/word_23/word_233.svg"
			},			
		]
	},	
	{
		letter:'u',
		words:[
			{
				index:0,
				word:"uıym",
				original:"ұйым" ,
				image:"/img/learn/word_24/word_241.svg"
			},
			{
				index:1,
				word:"un",
				original:"ұн" ,
				image:"/img/learn/word_24/word_242.svg"
			},			
			{
				index:2,
				word:"urlyq",
				original:"ұрлық" ,
				image:"/img/learn/word_24/word_243.svg"
			},			
		]
	},	
	{
		letter:'ú',
		words:[
			{
				index:0,
				word:"úı",
				original:"үй" ,
				image:"/img/learn/word_25/word_251.svg"
			},
			{
				index:1,
				word:"úırek",
				original:"үйрек" ,
				image:"/img/learn/word_25/word_252.svg"
			},			
			{
				index:2,
				word:"úrdis",
				original:"үрдіс" ,
				image:"/img/learn/word_25/word_253.svg"
			},			
		]
	},	
	{
		letter:'f',
		words:[
			{
				index:0,
				word:"fotobeıne",
				original:"фотобейне" ,
				image:"/img/learn/word_26/word_261.svg"
			},
			{
				index:1,
				word:"fotoshaqpaq",
				original:"фотошақпақ" ,
				image:"/img/learn/word_26/word_262.svg"
			},			
			{
				index:2,
				word:"fılmhana",
				original:"фильмхана" ,
				image:"/img/learn/word_26/word_263.svg"
			},			
		]
	},	

	{
		letter:'sh',
		words:[
			{
				index:0,
				word:"shalbar",
				original:"шалбар" ,
				image:"/img/learn/word_28/word_281.svg"
			},
			{
				index:1,
				word:"shaǵala",
				original:"шағала" ,
				image:"/img/learn/word_28/word_282.svg"
			},			
			{
				index:2,
				word:"shabdaly",
				original:"шабдалы" ,
				image:"/img/learn/word_28/word_283.svg"
			},			
		]
	},		
	{
		letter:'h',
		words:[
			{
				index:0,
				word:"haıýan",
				original:"хайуан" ,
				image:"/img/learn/word_27/word_271.svg"
			},
			{
				index:1,
				word:"halyq",
				original:"халық" ,
				image:"/img/learn/word_27/word_272.svg"
			},			
			{
				index:2,
				word:"hat",
				original:"хат" ,
				image:"/img/learn/word_27/word_273.svg"
			},			
		]
	},	
	{
		letter:'y',
		words:[
			{
				index:0,
				word:"yqpal",
				original:"ықпал" ,
				image:"/img/learn/word_29/word_291.svg"
			},
			{
				index:1,
				word:"ydys",
				original:"ыдыс" ,
				image:"/img/learn/word_29/word_292.svg"
			},			
			{
				index:2,
				word:"yrǵaq",
				original:"ырғақ" ,
				image:"/img/learn/word_29/word_293.svg"
			},			
		]
	},	
	{
		letter:'i',
		words:[
			{
				index:0,
				word:"iz",
				original:"із" ,
				image:"/img/learn/word_30/word_301.svg"
			},
			{
				index:1,
				word:"ish",
				original:"іш" ,
				image:"/img/learn/word_30/word_302.svg"
			},			
			{
				index:2,
				word:"ishý",
				original:"ішу" ,
				image:"/img/learn/word_30/word_303.svg"
			},			
		]
	},	
];