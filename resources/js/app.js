
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
		el: '#app'
});

//Функции для открытия и закрытия меню и модальных окон
$('#header__menu').click(function(event) {
	$('#left-menu').addClass('active');
});

$('#header__close').click(function(event) {
	$('#left-menu').removeClass('active');
});

$('#about__open').click(function(event) {
	$('#about').addClass('active');
});

$('#about__close').click(function(event) {
	$('#about').removeClass('active');
});


//Плавный scroll
	$("a").on('click', function(event) {

		if (this.hash !== "") {
			event.preventDefault();

			var hash = this.hash;

			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 800, function(){

				window.location.hash = hash;
			});
		} 
		$('#left-menu').removeClass('active');
	});

	//functions for translate
	$('#button-translate').click(function(event) {
		let result = '';
		let inputName = $('#name-input__input').val();

		if (String(inputName).match("^[a-zA-Z ]+$")){
			$('#main-content__title').html('Можно вводить только <span> кириллицу <span>')
		}
		else{

			inputName.split('').forEach((elem)=>{
				result += translate(elem);
			})

			$('#main-content__title').html('Ваше имя на латинице <span>'+result+'<span>')
		}


	});

	$('#research-translate').click(function(event) {
		let result = '';
		let inputName = $('#origin-text').val();

		if (String(inputName).match("^[a-zA-Z ]+$")){
			$('#translate-text').val('Можно вводить только кириллицу')
		}
		else{

			inputName.split('').forEach((elem)=>{
				result += translate(elem);
			})

			 $('#translate-text').val(result);
		}
	});


	function translate(word){
		let result = '';
		word=word.toLowerCase();

		switch(word){
			case 'а':
				result = 'a';
				break;
			case 'ә':
				result = 'á';
				break;        
			case 'б':
				result = 'b';
				break;        
			case 'в':
				result = 'v';
				break;        
			case 'г':
				result = 'g';
				break;        
			case 'ғ':
				result = 'ǵ';
				break;        
			case 'д':
				result = 'd';
				break;        
			case 'е':
				result = 'e';
				break;        
			case 'ж':
				result = 'j';
				break;        
			case 'з':
				result = 'z';
				break;        
			case 'и':
				result = 'ı';
				break;        
			case 'й':
				result = 'ı';
				break;        
			case 'к':
				result = 'k';
				break;        
			case 'қ':
				result = 'q';
				break;        
			case 'л':
				result = 'l';
				break;        
			case 'м':
				result = 'm';
				break;        
			case 'н':
				result = 'n';
				break;        
			case 'ң':
				result = 'ń';
				break;        
			case 'о':
				result = 'o';
				break;        
			case 'ө':
				result = 'ó';
				break;        
			case 'п':
				result = 'p';
				break;        
			case 'р':
				result = 'r';
				break;        
			case 'с':
				result = 's';
				break;        
			case 'т':
				result = 't';
				break;        
			case 'у':
				result = 'ý';
				break;        
			case 'ұ':
				result = 'u';
				break;        
			case 'ү':
				result = 'ú';
				break;        
			case 'ф':
				result = 'f';
				break;        
			case 'х':
				result = 'h';
				break;        
			case 'һ':
				result = 'h';
				break;        
			case 'ц':
				result = 'a';
				break;        
			case 'ч':
				result = 'ch';
				break;        
			case 'ш':
				result = 'sh';
				break;        
			case 'ы':
				result = 'y';
				break;  
			case 'і':
				result = 'i';
				break;                
			case ' ':
				result = ' ';
				break;        
			default:
				result = ''

		}

		return result;
	}

	function getRandomInt(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	function getQuestion(){
		let rand = getRandomInt(0,length-1);
		let question  = questions[rand].question;
		let answer  = questions[rand].answer;

		$('#test-question').text(question);

		return answer;    
	}

	//Часть с обработкой вопросов и тестированием
	var questions = require("./questions.js"); 
	let length = questions.length;
	
	let points = 0;
	let counts = 0;

	let answer=getQuestion();

	$('#test-submit').click(function(event) {

		let inputAnswer = $('#test-answer').val().toLowerCase();

		if(inputAnswer==answer){
				points++;
		}

		counts++;
		$('#question-answered').html(counts+1)

		if(counts<10){
			answer =getQuestion(); 
			$('#test-answer').val('');

		}
		else{

		 $('#test-question').css('display', 'none'); 
		 $('#test-question-title').css('display', 'none'); 
		 $('#test-name-input').css('display', 'none'); 

		 $('#result-points').text(points+'/10 баллов')

		 let advice = '';
		 if(points<=7){
				advice='Изучайте дальше. Мы уверены, вы можете сделать лучше';
		 }
		 else if(points==8){
				advice='У вас хорошие результаты, но вы можете лучше';

		 }
		 else if (points==9||points==10) {
				advice='Вы все превосходно знаете';

		 }
		 
		 $('#result-advice').text(advice)

		 $('#test-result').addClass('active')

		}


	});

	let check;

	$('.learn__alpha-item').click(function(event) {
		let letter = $(this).data('letter')

		let result = getWord(letter)
		
		if(check&&check[0]==result[0]){
			 result = getWord(letter)
		}
		check = result;

		$('#learn-image').attr('src',result[0])
		$('#learn-text').html('Слово для изучения:')
		$('#learn-word').html(result[1])
		$('#learn-original').html("("+result[2]+")")

	});

//Часть с обработкой вопросов и тестированием
var words = require("./words.js"); 

function getWord(letter){
	let image="";
	let text="";
	let original="";

	let item;
	
	words.forEach((each)=>{
		
		if(each.letter==letter){
			item= each.words[getRandomInt(0,2)];
			image = item.image;
			text = item.word
			original = item.original
		}

	})

	return [image,text,original];
}
