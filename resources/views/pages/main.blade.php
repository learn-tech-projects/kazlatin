@extends('pages.layouts.app')


@section('title', 'Главная')

@section('meta')
	<meta property="og:title" content="Kazlatin - все о латинице">
	<meta property="og:description" content="Узнайте своё имя на казахском языке в латинской транскрипции">
	<meta property="og:image" content="http://kaz-latinica.kz/img/lessons/les_1.jpg">
	<meta property="og:url" content="http://kaz-latinica.kz">
@endsection

@section('content')
<div class="container-fluid" style="padding:0px !important">

	
	<div  id="main-content" class="container-fluid video-block">

		<div class="overlay"></div>
		
		<div class="row video-block__row">
			<div class="col-md-12 video-block__content">
				
				<video class="video-block__video" autoplay muted loop id="myVideo">
				  <source src="/video/video_1.mp4" type="video/mp4">
				</video>

			</div>
		</div>

	</div>

	<div class="container-fluid main-logo">
		<div class="row">
			<div class="col-md-12 text-center">
				{{-- <img src="/img/content_1.png" alt=""> --}}
			</div>
		</div>
	</div>

	<div class="container-fluid main-content">
		<div class="row main-content__row">
			<div class="col-md-12 main-content__content">

				<p id="main-content__title" class="main-content__title">Узнайте своё имя на казахском языке в <span>латинской транскрипции</span> </p>

				<p class="main-content__text">В новом варианте отсутствуют апострофы и появились акуты (штрихи над буквами). Изменено написание букв Ә, И, Й, Ғ, Ң, Ө, Ү, У на латинице, а также введены диграфы (sh, ch)</p>

				<div class="name-input">
					<input id="name-input__input" placeholder="Ваше имя" type="text" class="name-input__input">
					<div id="button-translate" class="name-input__button">Перевести</div>
				</div>

				<!-- AddToAny BEGIN -->
				<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
					<a class="a2a_dd" href="https://www.addtoany.com/share"></a>
					<a class="a2a_button_facebook"></a>
					<a class="a2a_button_twitter"></a>
					<a class="a2a_button_whatsapp"></a>
					<a class="a2a_button_telegram"></a>
					<a class="a2a_button_copy_link"></a>
					<a class="a2a_button_vk"></a>
				</div>
				<!-- AddToAny END -->

			</div>
		</div>
	</div>

	@include('pages.layouts.main-partials.advantages')
	@include('pages.layouts.main-partials.lessons')
	@include('pages.layouts.main-partials.about')

	@section('scripts')
		<script async src="https://static.addtoany.com/menu/page.js"></script>
	@endsection

	

</div>
@endsection