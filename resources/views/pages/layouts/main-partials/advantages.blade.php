<div id="advantages" class="container-fluid advantages-block">
		<div class="row advantages-block__row">

			<div class="col-md-12">
				<p class="advantages-block__title">Почему стоит перейти на латиницу?</p>				
			</div>

			<div class="col-md-4 advantages-block__item-wrap">
				<div class="advantages-block__item">
					<img src="/img/adnvatages/adv_1.svg" alt="" class="advantages-block__image">
					<p class="advantages-block__text">"Латиница лучше подходит к тюркским языкам с точки зрения основ фонетической транскрипции"</p>

					<a target="_blank" href="https://bnews.kz/ru/news/v_chem_plusi_perehoda_na_latinitsu" class="advantages-block__button">Узнать больше</a>
				</div>
			</div>

			<div class="col-md-4 advantages-block__item-wrap">
				<div class="advantages-block__item">
					<img src="/img/adnvatages/adv_22.svg" alt="" class="advantages-block__image">
					<p class="advantages-block__text">"Мировая интеграция"</p>

					<a target="_blank" href="https://yvision.kz/post/763371" class="advantages-block__button">Узнать больше</a>
				</div>			
			</div>

			<div class="col-md-4 advantages-block__item-wrap">
				<div class="advantages-block__item">
					<img src="/img/adnvatages/adv_3.svg" alt="" class="advantages-block__image">
					<p class="advantages-block__text">"Объединение тюркского мира"</p>

					<a target="_blank" href="http://www.diapazon.kz/aktobe/aktobe-theme-of-thet-day/95415-latinica-po-kazahski-plyusy-i-minusy.html" class="advantages-block__button">Узнать больше</a>
				</div>			
			</div>

		</div>
	</div>
