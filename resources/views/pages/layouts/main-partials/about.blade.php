<div id="about" class="container-fluid about">
	<div class="row about__row">

		<div class="col-md-12 about__content">

			<img id="about__close" class="about__close" src="/img/close.svg" alt="">
		
		</div>

		<div class="col-md-12 about__title-wrap">
			<p class="about__title"> О сайте <span>kaz-latinica.kz</span></p>
		</div>

		<div class="col-lg-3 col-xl-2 offset-xl-4 offset-lg-3 col-sm-12 offset-0 about__text-wrap">

			<p class="about__text">Данный сайт сделан для того, чтобы каждый желающий мог ознакомиться с новым алфавитом и узнать что то новое для себя</p>
			<p class="about__text">Если вы нашли ошибку или у вас есть предложения, то вы связаться с нами по следующим контактным данным:</p>

		</div>

		<div class="col-lg-3 col-xl-2  col-sm-12  about__text-wrap">
			
			{{-- <div class="about__contacts">
				<img class="about__contacts-icon" src="/img/about/phone.svg" alt="">
				<p class="about__contacts-text">Телефон</p>
			</div>

			<p class="about__text">Телефон: <a href="tel:+1866496-3250">+7 (777) 777-77777</a></p> --}}

			<div class="about__contacts">
				<img class="about__contacts-icon" src="/img/about/email.svg" alt="">
				<p class="about__contacts-text">Email</p>
			</div>

			<p class="about__text">Email: <a href="mailto:mail@gmail.com">kazlatinica@gmail.com</a></p>			

			{{-- <div class="about__contacts">
				<img class="about__contacts-icon" src="/img/about/telegram.svg" alt="">
				<p class="about__contacts-text">Telegram</p>
			</div>

			<p class="about__text">Telegram: <a href="telegram">@telegram</a></p> --}}			

		</div>

	</div>
</div>