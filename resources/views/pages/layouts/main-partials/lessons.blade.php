<div id="lessons" class="lessons-block container-fluid">

		<div class="lessons-block__video">
			<div class="overlay"></div>
			<video autoplay muted loop id="myVideo">
			  <source src="/video/video_2.mp4" type="video/mp4">
			</video>
		</div>

		<div class="row lessons-block__row">

			<div class="col-md-12 lessons-block__title-wrap">
				<p class="lessons-block__title">Улучшить и проверить свои знания по <span>латинскому</span></p>
			</div>

		<a href="/learn" class="col-md-4 lessons-block__item-wrap">
				<div class="lessons-block__item">

					<div class="lessons-block__item-header">
						<p class="lessons-block__item-title">Обучение</p>
						<hr class="lessons-block__item-hr">
					</div>

					<div class="lessons-block__item-content" style="background-image: url('/img/lessons/les_3.jpg');">
						<div class="lessons-block__overlay">
							<img src="/img/lessons/icon_3.svg" alt="">
						</div>
					</div>

				</div>
			</a>			



			<a href="/research" class="col-md-4 lessons-block__item-wrap">
				<div class="lessons-block__item">

					<div class="lessons-block__item-header">
						<p class="lessons-block__item-title">Изучить правила и посмотреть перевод(Онлайн переводчик)</p>
						<hr class="lessons-block__item-hr">
					</div>

					<div class="lessons-block__item-content" style="background-image: url('/img/lessons/les_1.jpg');">
						<div class="lessons-block__overlay">
							<img src="/img/lessons/icon_1.svg" alt="">
						</div>
					</div>

				</div>
			</a>

			<a href="/test" class="col-md-4 lessons-block__item-wrap">
				<div class="lessons-block__item">

					<div class="lessons-block__item-header">
						<p class="lessons-block__item-title">Протестировать свои знания онлайн</p>
						<hr class="lessons-block__item-hr">
					</div>

					<div class="lessons-block__item-content" style="background-image: url('/img/lessons/les_2.jpg');">
						<div class="lessons-block__overlay">
							<img src="/img/lessons/icon_2.svg" alt="">
						</div>
					</div>

				</div>
			</a>			

		</div>
	</div>
