<div class="menu" id="left-menu">
	<img id="header__close" class="menu__close" src="/img/close.svg" alt="">

	<div class="menu__container">

		<img class="menu__logo" src="/img/kazlatin-logo.png" alt="">
		
		<a href="#main-content" class="menu__item">
			<p class="menu__item-text">Узнать свое имя на латинском</p>
		</a>

		<a href="#advantages" class="menu__item">
			<p class="menu__item-text">Почему стоит перейти на латинский</p>
		</a>		

		<a href="#lessons" class="menu__item">
			<p class="menu__item-text">Улучшить и проверить свои знания</p>
		</a>

		<a id="about__open" class="menu__item">
			<p class="menu__item-text">О нас</p>
		</a>				
	
	</div>
</div>