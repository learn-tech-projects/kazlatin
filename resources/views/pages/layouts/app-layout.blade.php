<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title') — Kazlatin site</title>

        <meta name="description" content="@yield('description')">
        <meta name="keywords" content="@yield('keywords')">
        
        <meta name="csrf-token" content="{{csrf_token()}}">

        <meta name="description" content="На данной странице вы можете воспользоваться онлайн переводчиком Казахского языка с кириллицы на латиницу и наоборот, а также ознакомиться с новым алфавитом, основанным на латинской графике.">
        <meta name="keywords" content="латиница казахстан латинская транскрипция казахстан-2030">
        <link rel="canonical" href="http://kaz-latinica.kz" />

        <meta itemprop="name" content="Kazlatin site">
        <meta itemprop="description" content="На данной странице вы можете воспользоваться онлайн переводчиком Казахского языка с кириллицы на латиницу и наоборот, а также ознакомиться с новым алфавитом, основанным на латинской графике.">
        <meta itemprop="image" content="http://kaz-latinica.kz/img/lessons/les_2.jpg">

        <meta name="twitter:card" content="summary">
        <meta name="twitter:url" content="http://kaz-latinica.kz">
        <meta name="twitter:title" content="Kazlatin site">
        <meta name="twitter:description" content="На данной странице вы можете воспользоваться онлайн переводчиком Казахского языка с кириллицы на латиницу и наоборот, а также ознакомиться с новым алфавитом, основанным на латинской графике.">
        <meta name="twitter:image" content="http://kaz-latinica.kz/img/lessons/les_2.jpg">

        <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
        <link rel="manifest" href="/img/site.webmanifest">
        <link rel="mask-icon" href="/img/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        @yield('css')

        <title>Cuisine-master</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="css/app.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">

        @yield('meta')
        
    </head>
    <body>
		<div id="app">
        	@yield('content')
		</div>

        <script src="js/app.js"></script> 
        <script src="js/owl.carousel.min.js"></script> 
        <script src="js/jquery-3.3.1.js"></script> 
        <script src="js/bootstrap.min.js"></script> 
        @include('pages.layouts.partials.stats')  

    </body>
</html>
