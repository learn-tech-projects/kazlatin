@extends('pages.layouts.app-layout')


@section('title', 'Изучить')

@section('meta')
	<meta property="og:title" content="Kazlatin - перевод на латиницу">
	<meta property="og:description" content="Онлайн перевод с кириллицы на латиницу">
	<meta property="og:image" content="http://kaz-latinica.kz/img/lessons/les_2.jpg">
	<meta property="og:url" content="http://kaz-latinica.kz">
@endsection

@section('content')
<div class="container-fluid" style="padding:0px !important">


	<div  class="container-fluid research active">
		<div class="row research__row">

			<a class="research__close" href="/">На главную</a>

			<div class="col-md-12 research__title-wrap">
				<p class="research__title"> Онлайн перевод  <span>с кириллицы на латиницу</span></p>
			</div>

			<div class="col-md-10 offset-md-1 research__text-wrap">

				<p class="research__text">Указом Президента Республики Казахстан от 26 октября 2017 года №569 Казахский язык будет поэтапно переведен с кириллической графики на латинскую до 2025 года.</p>

				<p class="research__text">На данной странице вы можете воспользоваться онлайн переводчиком Казахского языка с кириллицы на латиницу и наоборот, а также ознакомиться с новым алфавитом, основанным на латинской графике.</p>

				<div class="research__input-block">
					<textarea placeholder="Изначальный текст" class="research__input" name="origin-text" id="origin-text" ></textarea>

					<textarea placeholder="Результат" class="research__input" name="translate-text" id="translate-text" ></textarea>

				</div>

				<div class="research__buttons-block">
					<div id="research-translate" class="research__button">Перевести с кириллицы на латиницу</div>
				</div>

			</div>

			

		</div>


		<div class="row research__table">
			<div class="col-md-8 col-sm-12 offset-md-2" >

				<table class="table table-bordered table-responsive">
				  <thead>
				    <tr>
				      <th scope="col">Современная кириллица	</th>
				      <th scope="col">Латиница 2017 Первоначальная версия	</th>
				      <th scope="col">Латиница 2017 Вторая версия	</th>
				      <th scope="col">Латиница 2018 Утверждённая версия	</th>
				      <th scope="col">Транскрипция МФА</th>
				    </tr>
				  </thead>
				  <tbody>
			        <tr>
			            <td>Аа</td>
			            <td>Aa</td>
			            <td>Aa</td>
			            <td>Aa</td>
			            <td>[ɑ]</td>
			        </tr>
			        <tr>
			            <td>Әә</td>
			            <td>Ae ae</td>
			            <td>Aʼaʼ</td>
			            <td>Áá</td>
			            <td>[æ]</td>
			        </tr>
			        <tr>
			            <td>Бб</td>
			            <td>Bb</td>
			            <td>Bb</td>
			            <td>Bb</td>
			            <td>[b]</td>
			        </tr>
			        <tr>
			            <td>Вв</td>
			            <td>Vv</td>
			            <td>Vv</td>
			            <td>Vv</td>
			            <td>[v]</td>
			        </tr>
			        <tr>
			            <td>Гг</td>
			            <td>Gg</td>
			            <td>Gg</td>
			            <td>Gg</td>
			            <td>[ɡ]</td>
			        </tr>
			        <tr>
			            <td>Ғғ</td>
			            <td>Gh gh</td>
			            <td>Gʼgʼ</td>
			            <td>Ǵǵ</td>
			            <td>[ʁ], [ɣ]</td>
			        </tr>
			        <tr>
			            <td>Дд</td>
			            <td>Dd</td>
			            <td>Dd</td>
			            <td>Dd</td>
			            <td>[d]</td>
			        </tr>
			        <tr>
			            <td>Ее</td>
			            <td>Ee</td>
			            <td>Ee</td>
			            <td>Ee</td>
			            <td>[e], [je]</td>
			        </tr>
			        <tr>
			            <td>Ёё</td>
			            <td></td>
			            <td></td>
			            <td></td>
			            <td>[jɔ], [jɵ]</td>
			        </tr>
			        <tr>
			            <td>Жж</td>
			            <td>Zh zh</td>
			            <td>Jj</td>
			            <td>Jj</td>
			            <td>[ʐ], [ʒ]</td>
			        </tr>
			        <tr>
			            <td>Зз</td>
			            <td>Zz</td>
			            <td>Zz</td>
			            <td>Zz</td>
			            <td>[z]</td>
			        </tr>
			        <tr>
			            <td>Ии</td>
			            <td>Ii</td>
			            <td>Iʼiʼ</td>
			            <td>Iı</td>
			            <td>[ɯj], [ɘj]</td>
			        </tr>
			        <tr>
			            <td>Йй</td>
			            <td>Jj</td>
			            <td>Iʼiʼ</td>
			            <td>Iı</td>
			            <td>[j]</td>
			        </tr>
			        <tr>
			            <td>Кк</td>
			            <td>Kk</td>
			            <td>Kk</td>
			            <td>Kk</td>
			            <td>[k]</td>
			        </tr>
			        <tr>
			            <td>Ққ</td>
			            <td>Qq</td>
			            <td>Qq</td>
			            <td>Qq</td>
			            <td>[q]</td>
			        </tr>
			        <tr>
			            <td>Лл</td>
			            <td>Ll</td>
			            <td>Ll</td>
			            <td>Ll</td>
			            <td>[ɫ]</td>
			        </tr>
			        <tr>
			            <td>Мм</td>
			            <td>Mm</td>
			            <td>Mm</td>
			            <td>Mm</td>
			            <td>[m]</td>
			        </tr>
			        <tr>
			            <td>Нн</td>
			            <td>Nn</td>
			            <td>Nn</td>
			            <td>Nn</td>
			            <td>[n]</td>
			        </tr>
			        <tr>
			            <td>Ңң</td>
			            <td>Ng ng</td>
			            <td>Nʼnʼ</td>
			            <td>Ńń</td>
			            <td>[ŋ]</td>
			        </tr>
			        <tr>
			            <td>Оо</td>
			            <td>Oo</td>
			            <td>Oo</td>
			            <td>Oo</td>
			            <td>[ɔ]</td>
			        </tr>
			        <tr>
			            <td>Өө</td>
			            <td>Oe oe</td>
			            <td>Oʼoʼ</td>
			            <td>Óó</td>
			            <td>[ɵ]</td>
			        </tr>
			        <tr>
			            <td>Пп</td>
			            <td>Pp</td>
			            <td>Pp</td>
			            <td>Pp</td>
			            <td>[p]</td>
			        </tr>
			        <tr>
			            <td>Рр</td>
			            <td>Rr</td>
			            <td>Rr</td>
			            <td>Rr</td>
			            <td>[r]</td>
			        </tr>
			        <tr>
			            <td>Сс</td>
			            <td>Ss</td>
			            <td>Ss</td>
			            <td>Ss</td>
			            <td>[s]</td>
			        </tr>
			        <tr>
			            <td>Тт</td>
			            <td>Tt</td>
			            <td>Tt</td>
			            <td>Tt</td>
			            <td>[t]</td>
			        </tr>
			        <tr>
			            <td>Уу</td>
			            <td>Ww</td>
			            <td>Yʼyʼ</td>
			            <td>Ýý</td>
			            <td>[w], [ɯw], [ɘw]</td>
			        </tr>
			        <tr>
			            <td>Ұұ</td>
			            <td>Uu</td>
			            <td>Uu</td>
			            <td>Uu</td>
			            <td>[ʊ]</td>
			        </tr>
			        <tr>
			            <td>Үү</td>
			            <td>Ue ue</td>
			            <td>Uʼuʼ</td>
			            <td>Úú</td>
			            <td>[ʉ]</td>
			        </tr>
			        <tr>
			            <td>Фф</td>
			            <td>Ff</td>
			            <td>Ff</td>
			            <td>Ff</td>
			            <td>[f]</td>
			        </tr>
			        <tr>
			            <td>Хх</td>
			            <td>Hh</td>
			            <td>Hh</td>
			            <td>Hh</td>
			            <td>[x]</td>
			        </tr>
			        <tr>
			            <td>Һһ</td>
			            <td>Hh</td>
			            <td>Hh</td>
			            <td>Hh</td>
			            <td>[h]</td>
			        </tr>
			        <tr>
			            <td>Цц</td>
			            <td>Cc</td>
			            <td></td>
			            <td></td>
			            <td>[ʦ]</td>
			        </tr>
			        <tr>
			            <td>Чч</td>
			            <td>Ch ch</td>
			            <td>Cʼcʼ</td>
			            <td>Ch ch</td>
			            <td>[ʧ]</td>
			        </tr>
			        <tr>
			            <td>Шш</td>
			            <td>Sh sh</td>
			            <td>Sʼsʼ</td>
			            <td>Sh sh</td>
			            <td>[ʃ], [ʂ]</td>
			        </tr>
			        <tr>
			            <td>Щщ</td>
			            <td></td>
			            <td></td>
			            <td></td>
			            <td>[ʃʃ], [ʂʂ]</td>
			        </tr>
			        <tr>
			            <td>Ъъ</td>
			            <td></td>
			            <td></td>
			            <td></td>
			            <td></td>
			        </tr>
			        <tr>
			            <td>Ыы</td>
			            <td>Yy</td>
			            <td>Yy</td>
			            <td>Yy</td>
			            <td>[ɯ]</td>
			        </tr>
			        <tr>
			            <td>Іі</td>
			            <td>Ii</td>
			            <td>Ii</td>
			            <td>Ii</td>
			            <td>[ɘ]</td>
			        </tr>
			        <tr>
			            <td>Ьь</td>
			            <td></td>
			            <td></td>
			            <td></td>
			            <td></td>
			        </tr>
			        <tr>
			            <td>Ээ</td>
			            <td></td>
			            <td></td>
			            <td></td>
			            <td>[e]</td>
			        </tr>
			        <tr>
			            <td>Юю</td>
			            <td></td>
			            <td></td>
			            <td></td>
			            <td>[jɯw], [jɘw]</td>
			        </tr>
			        <tr>
			            <td>Яя</td>
			            <td></td>
			            <td></td>
			            <td></td>
			            <td>[jɑ], [jæ]</td>
			        </tr>
			    </tbody>
				</table>

			</div>
		</div>
	</div>
	

	

</div>
@endsection