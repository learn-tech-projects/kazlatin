@extends('pages.layouts.app-layout')


@section('title', 'Изучить')

@section('meta')
	<meta property="og:title" content="Kazlatin - изучить латиницу">
	<meta property="og:description" content="На этой странице вы можете ознакомиться с основными словами для того, чтобы улучшить свои знания по латинской транскрипции">
	<meta property="og:image" content="http://kaz-latinica.kz/img/lessons/les_1.jpg">
	<meta property="og:url" content="http://kaz-latinica.kz">
@endsection


@section('content')
<div class="container-fluid" style="padding:0px !important">

	
	<div  class="container-fluid learn active">

		<div class="row learn__row">

			<a class="learn__close" href="/">На главную</a>

			<div class="col-md-12 learn__title-wrap">
				<p class="learn__title"> Освоить  <span> латиницу</span></p>
			</div>

			<div class="col-md-10 offset-md-1 learn__text-wrap">

				<p class="learn__text">На этой странице вы можете ознакомиться с основными словами для того, чтобы улучшить свои знания по латинской транскрипции</p>

			</div>

		</div>


		<div class="row">
			
			<div class="col-md-6 col-sm-12 order-sm-last order-md-1 order-last learn__alpha-wrap">
				
				<div data-letter="a" class="learn__alpha-item">Aa</div>
				<div data-letter="á" class="learn__alpha-item">Áá</div>
				<div data-letter="b" class="learn__alpha-item">Bb</div>
				<div data-letter="v" class="learn__alpha-item">Vv</div>
				<div data-letter="g" class="learn__alpha-item">Gg</div>
				<div data-letter="ǵ" class="learn__alpha-item">Ǵǵ</div>
				<div data-letter="d" class="learn__alpha-item">Dd</div>
				<div data-letter="e" class="learn__alpha-item">Ee</div>
				<div data-letter="j" class="learn__alpha-item">Jj</div>
				<div data-letter="z" class="learn__alpha-item">Zz</div>
				<div data-letter="ı" class="learn__alpha-item">Iı</div>
				<div data-letter="k" class="learn__alpha-item">Kk</div>
				<div data-letter="q" class="learn__alpha-item">Qq</div>
				<div data-letter="l" class="learn__alpha-item">Ll</div>
				<div data-letter="m" class="learn__alpha-item">Mm</div>
				<div data-letter="n" class="learn__alpha-item">Nn</div>
				<div data-letter="ń" class="learn__alpha-item">Ńń</div>
				<div data-letter="o" class="learn__alpha-item">Oo</div>
				<div data-letter="ó" class="learn__alpha-item">Óó</div>
				<div data-letter="p" class="learn__alpha-item">Pp</div>
				<div data-letter="r" class="learn__alpha-item">Rr</div>
				<div data-letter="s" class="learn__alpha-item">Ss</div>
				<div data-letter="t" class="learn__alpha-item">Tt</div>
				<div data-letter="ý" class="learn__alpha-item">Ýý</div>
				<div data-letter="u" class="learn__alpha-item">Uu</div>
				<div data-letter="ú" class="learn__alpha-item">Úú</div>
				<div data-letter="f" class="learn__alpha-item">Ff</div>
				<div data-letter="h" class="learn__alpha-item">Hh</div>
				{{-- <div data-letter="ch" class="learn__alpha-item">Ch</div> --}}
				<div data-letter="sh" class="learn__alpha-item">Sh</div>
				<div data-letter="y" class="learn__alpha-item">Yy</div>
				<div data-letter="i" class="learn__alpha-item">Ii</div>


			</div>

			<div class="col-md-6 col-sm-12 order-sm-1 order-1 order-md-last learn__result-wrap">

				<div class="learn__result-item">
					
					<img id="learn-image" src="/img/learn/learn_8.svg" alt="" class="learn__result-image">
					<p class="learn__result-text">
						<span id="learn-text"> Нажмите кнопку чтобы найти нужное </span>
						<span id="learn-word">слово</span>
						<span id="learn-original"></span>
					</p>

				</div>

			</div>

		</div>
	</div>
	

	

</div>
@endsection