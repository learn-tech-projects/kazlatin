@extends('pages.layouts.app-layout')


@section('title', 'Протестировать свои знания')

@section('meta')
	<meta property="og:title" content="Kazlatin - перевод на латиницу">
	<meta property="og:description" content="Протестировать свои  знания латиницы">
	<meta property="og:image" content="http://kaz-latinica.kz/img/lessons/les_3.jpg">
	<meta property="og:url" content="http://kaz-latinica.kz">
@endsection

@section('content')
<div class="container-fluid" style="padding:0px !important">


	<div  class="container-fluid test active">
		<div class="row test__row">

			<a class="test__close" href="/">На главную</a>

			<div class="col-md-12 test__title-wrap">
				<p class="test__title"> Протестировать свои  знания <span> латиницы</span></p>
			</div>

			<div class="col-md-10 offset-md-1 test__text-wrap ">

				<div class="test__icon-wrap">
					<img src="/img/folder.svg" class="test__icon" alt="">
				</div>

				<p class="test__text text-center">На данной странице вы можете опробывать свои знания по переводу слов на латиницу и узнать свой результат</p>

				<p id="test-question-title" class="test__text question-title">
					Вопрос[
					<span id="question-answered">1</span>
					/10]
					:
				</p>

				<p id="test-question" class="test__question text-center">Ожидание вопроса</p>

				<div id="test-name-input" class="name-input">
					<input id="test-answer" placeholder="Ответ" type="text" class="name-input__input">
					<div id="test-submit" class="name-input__button">Ответить</div>
				</div>

				<div id="test-result" class="test__result">
					<p class="test__result-title">Ваш результат:</p>
					<p class="test__result-text"> <span id="result-points">4/5</span> баллов</p>

					<p class="test__result-advice" id="result-advice"></p>
				</div>
				

			</div>

			

		</div>


		
	</div>
	

	

</div>
@endsection